package ru.pisarev.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.bootstrap.Bootstrap;

import static ru.pisarev.tm.constant.BackupConst.BACKUP_LOAD;
import static ru.pisarev.tm.constant.BackupConst.BACKUP_SAVE;

public class Backup extends Thread {

    @NotNull
    private static final int INTERVAL = 30000;

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.runCommand(BACKUP_SAVE);
    }

    public void load() {
        bootstrap.runCommand(BACKUP_LOAD);
    }

}
