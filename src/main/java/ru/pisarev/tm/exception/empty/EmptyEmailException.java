package ru.pisarev.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    @NotNull
    public EmptyEmailException() {
        super("Error. Email is empty");
    }

}
