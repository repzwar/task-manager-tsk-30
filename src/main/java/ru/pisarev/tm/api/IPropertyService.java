package ru.pisarev.tm.api;

import ru.pisarev.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    String getApplicationVersion();

}
