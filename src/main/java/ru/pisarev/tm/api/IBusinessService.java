package ru.pisarev.tm.api;

import ru.pisarev.tm.model.AbstractBusinessEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    List<E> findAll(final String userId);

    List<E> findAll(final String userId, final Comparator<E> comparator);

    void addAll(final String userId, final Collection<E> collection);

    E add(final String userId, final E entity);

    E findById(final String userId, final String id);

    void clear(final String userId);

    E removeById(final String userId, final String id);

    E remove(final String userId, final E entity);

}
